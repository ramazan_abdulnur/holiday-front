 let options = {
  root: null,
  rootMargin: '0px',
  threshold: [1,0.8]
}


let callback = (entries, observer) => {
  entries.forEach(entry => {
        
    if (entry.isIntersecting) {
      if(entry.target.getElementsByTagName('source')[0].src ===entry.target.getElementsByTagName('source')[0].baseURI){
        entry.target.getElementsByTagName('source')[0].src = entry.target.dataset.src
        entry.target.load();
      }
      
        entry.target.removeAttribute('poster')
        entry.target.play();
        console.dir(entry.target.getElementsByTagName('source')[0].src);
    }
    else {
        entry.target.pause();
    }
    

  });
}


let fadeText = (et, observer) => {
  et.forEach(entry => {
        
    if (entry.isIntersecting) {
        $(entry.target).addClass('visible')
    }
    else {
        $(entry.target).removeClass('visible')
    }   

  });
}

let observer = new IntersectionObserver(callback, options);

let textObserver = new IntersectionObserver(fadeText, options);

const videos = document.querySelectorAll(".obs-video");

const texts = document.querySelectorAll(".scroll-fade");



videos.forEach(video => {
  observer.observe(video);
});
texts.forEach(t => {
  textObserver.observe(t);
});
